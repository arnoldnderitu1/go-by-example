# Pointers
A pointer in GO is a variable that holds the **memory address** of another variable.

A variable stores some data at a particular memory address in the system. 

A memory address is always found in hexadecimal format (starting with 0x like 0xFFAAF etc.)

```
i, j := 42, 2701

p := &i // point to i
fmt.Println(*p) // read the value of i through the pointer
fmt.Println(p) // read the memory address of i referenced by the pointer
*p = 21 // set i through the pointer
fmt.Println(i) // see the new value of i

p = &j // point to j
*p = *p / 37 // divide j through the pointer and set the new value of j through the pointer
fmt.Println(j) // see the new value of j
```

The output for the above block of code:
```
42
0xc000018050
21
73
```

The `*` denotes the pointer's underlying value.

This is known as "dereferencing" or "indirecting"

Unlike C, GO has no pointer arithmetic.

## What is the need of pointers?
A variable is the name given to a memory allocation where the actual data is stored.

To access the stored data we need the address of that particular memory location. 

A pointer not only stores the memory address of a variable, it also points to where the memory is located and provides ways to find out the value stored at that memory location. 

In GO, everything is **passed by value**. Every time you pass something to a function, the function gets a value, but it doesn't have access to the original object. 

The code block below demonstrates this:

```
package main

import (
	"fmt"
)

type User struct {
	Name string
	Pets []string
}

func (u User) newPet() {
	u.Pets = append(u.Pets, "Lucy")
	fmt.Println(u)
}

func main() {
	u := User{Name: "Anna", Pets: []string{"Bailey"}}
	u.newPet() // {Anna [Bailey Lucy]} -- the user with a new pet, Lucy
	fmt.Println(u)
}
```

The function `newPet()` gives a user a new pet, "Lucy". And on the main function we create a new user "Anna".

The pet "Lucy" is successfully appended to Anna's pet. But when we print the user on the following line, the new pet is missing!

We have passed a **value** of a user to newPet() and gave her a new pet. We still haven't changed (or **mutated**) the original user. If we go back to our code we could write it like this:

```
func (u2 User) newPetTwo() {
	u2.Pets = append(u2.Pets, "Lucy")
	fmt.Println(u2)
}

func main() {
	u := User{Name: "Anna", Pets: []string{"Bailey"}}
	u.newPet()
	fmt.Println(u)
}
```
But that we'll still give the user one pet.

Now let's try this:
```
package main
import (
 "fmt"
)
type User struct {
 Name string
 Pet  []string
}
func (p2 *User) newPet() {
 fmt.Println(*p2, "underlying value of p2 before")
 p2.Pet = append(p2.Pet, "Lucy")
 fmt.Println(*p2, "underlying value of p2 after")
}
func main() {
u := User{Name: "Anna", Pet: []string{"Bailey"}} // this time we'll generate a pointer!
 fmt.Println(u, "u before")
p := &u // Let's make a pointer to u!
p.newPet()
 fmt.Println(u, "u after")
// Does Anna have a new pet now? Yes!
}
```
