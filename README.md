# Go by Example
This is a hands-on look at Go using annotated example programs. Browse through the list below.

* [Structs](structs.md)
* [Pointers](pointers.md)
* [HTTP Servers](servers.md)