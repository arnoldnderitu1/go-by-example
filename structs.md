# Structs
Typed collections of fields. Useful for grouping data together to form records.

1. Declaring a `person` struct type with `name` and `age` fields.
```
type person struct {
	name string
	age int
}
```

2. Function to construct a new person struct with a given name. <br>You can safely return a `pointer` to local variable. A local variable will survive the scope of the function.

> A [pointer](pointers.md) holds the memory address of a value.

```
func newPerson(name string) *person {
	p := person{name: name}
	p.age = 42
	return &p
}
```