# HTTP Servers
To write a HTTP server in GO we use the `net/http` package.

A fundamental concept in `net/http` servers is *handlers*.

A handlers is an object implementing the `http`. A handler interface.

A common way to write a handler is by using the `http.HandlerFunc` adapter on functions with the appropriate signature.

Functions serving as handlers take a `http.ResponseWriter` and a `http.Request` as arguments. The response write is used to fill in the HTTP response. Here our simple response is just **"hello\n"**

```
func hello(w http.ResponseWriter, req *http.Request){
    fmt.FPrintf(w, "hello\n")
}
```

This other handler does something a little more sophisticated by reading all the HTTP request headers and echoing them into the response body.

```
func headers(w http.ResponseWriter, req *http.Request) {
    for name, headers := range req.Header {
        for _, h := range header {
            fmt.Fprintf(w, "%v:%v\n", name, h)
        }
    }
}
```

We register our handlers on server routes using `http.HandleFunc` convenience function. This function sets up the *default router* in the `net/http` package and takes a function as an argument.

Finally, we call the `ListenAndServe` with the port and a handler. `nil` tells it to use the default router we've just set up

```
func main(){
    http.HandleFunc("/hello", hello)
    http.HandleFunc("/headers", headers)

    http.ListenAndServe(":8090", nil)
}
```

For the full code:
> See [http-servers.go](HTTPServer/http-servers.go)

To run the file:
```
go run http-servers.go
```

To access the `/hello` route

```
curl localhost:8090/hello
```

To access the `/headers` route

```
curl localhost:8090/headers
```