package main

import (
	"fmt"
)

func noPointer() string {
	return "string"
}

func pointerTest() *string {
	return nil // you cannot return nil from a function that returns a string

	// but you can return nil from a function that returns a pointer to a string!

	// The zero value of a pointer is nil
}

func pointerTestTwo() *string {
	s := "string" // &"string" doesn't work
	return &s

	// returns the memory address of s
}

func main() {
	fmt.Println(noPointer())      // prints string
	fmt.Println(pointerTest())    // prints <nil>
	fmt.Println(pointerTestTwo()) // prints something like 0x40c158 (an address memory)
	s := pointerTestTwo()         // now s holds an address in memory for a variable
	fmt.Println(s)                // prints something like 0x40c158
	sp := *s                      // sp now holds the value found at the address that s holds
	fmt.Println(sp)               // string
}
