package main

import (
	"fmt"
)

type User struct {
	Name string
	Pets []string
}

func (u User) newPet() {
	u.Pets = append(u.Pets, "Lucy")
	fmt.Println(u)
}

func (p2 *User) newPet2() {
	fmt.Println(*p2, "underlying value of p2 before")
	p2.Pets = append(p2.Pets, "Lucy")
	fmt.Println(*p2, "underlying value of p2 after")
}

func main() {
	u := User{Name: "Anna", Pets: []string{"Bailey"}}
	fmt.Println(u, "u before")
	p := &u
	p.newPet() // {Anna [Bailey Lucy]} -- the user with a new pet, Lucy
	fmt.Println(u, "u after")
}
